#ifndef UTILS_H
#define UTILS_H

#include <QProcess>

//Перечень используемых видов шифрования
enum {
    STD_OPN		= 0x0001,
    STD_WEP		= 0x0002,
    STD_WPA		= 0x0004,
    STD_WPA2	= 0x0008,

    STD_FIELD	= (STD_OPN | STD_WEP | STD_WPA | STD_WPA2),

    ENC_WEP		= 0x0010,
    ENC_TKIP	= 0x0020,
    ENC_WRAP	= 0x0040,
    ENC_CCMP	= 0x0080,
    ENC_WEP40	= 0x1000,
    ENC_WEP104	= 0x0100,

    ENC_FIELD	= (ENC_WEP | ENC_TKIP | ENC_WRAP | ENC_CCMP | ENC_WEP40 | ENC_WEP104),

    AUTH_OPN	= 0x0200,
    AUTH_PSK	= 0x0400,
    AUTH_MGT	= 0x0800,

    AUTH_FIELD	= (AUTH_OPN | AUTH_PSK | AUTH_MGT)
};

//Структура файла рукопожатия
struct WPA_handshake
{
    char essid[36];

    unsigned char bssid[6], stmac[6];
    unsigned char snonce[32], anonce[32];

    unsigned char eapol[256];
    unsigned int eapol_size;

    int keyver;
    unsigned char keymic[16];
};

/*
 * Запускает процесс командной строкой command, ждет msecs миллисекунд и
 * возращает список строк, которые выдал процесс в консоль
 */
QStringList callAndWaitProcess(const QString &command, int msecs = 1000);

/*
 * Проверяет заполнен ли контейнер buf одинаковыми значениями val
 */
template<typename A, typename C>
bool isFilledWith(const A& buf, size_t len, C val)
{
    for(size_t i = 0; i < len; i++)
        if(buf[i] != val) return false;
    return true;
}

/*
 * Проверяет совпадают ли значения в двух контейнерах
 */
template<typename A, typename B>
bool equalArray(const A& a, const B& b, size_t len)
{
    for(size_t i = 0; i < len; i++)
        if(a[i] != b[i]) return false;
    return true;
}

#endif // UTILS_H
