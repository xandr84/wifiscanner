# WiFi Scanner #

The scanner listens for 802.11 protocol packets, catching WPA2 handshakes, saves them and allows you to select a password on the network in a distributed way on computers and video cards

![wifiscan1.png](https://bitbucket.org/repo/9kqgad/images/3623707076-wifiscan1.png)
![wifiscan2.png](https://bitbucket.org/repo/9kqgad/images/3371109048-wifiscan2.png)