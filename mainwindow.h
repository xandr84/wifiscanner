#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QTimer>
#include <QMap>
#include <QPair>
#include <QTableWidget>
#include <QTreeWidget>
#include <QLabel>
#include <QFile>
#include <QTime>
#include <tins/tins.h>
#include "utils.h"

namespace Ui {
class MainWindow;
}

using namespace Tins;

//Главное окно программы
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void onScanButton(bool checked);    //Обработчик кнопки "Сканировать"
    void onDeauthButton(bool checked);  //Обработчик кнопки "Реассоциировать"
    void onAboutHSButton();             //Обработчик кнопки "Описание..." в разделе рукопожатий
    void onDelHSButton();               //Обработчик кнопки "Удалить" в разделе рукопожатий
    void onAddHostButton();             //Обработчик кнопки "Добавить" в перечне компьютеров
    void onDelHostButton();             //Обработчик кнопки "Удалить" в перечне компьютеров
    void onEditHostButton();            //Обработчик кнопки "Изменить" в перечне компьютеров
    void onStartSearchButton();         //Обработчик кнопки "Подготовка" в перечне компьютеров
    void onDoSearchButton();            //Обработчик кнопки "Перебор" в перечне компьютеров
    void onStopSearchButton();          //Обработчик кнопки "Остановка" в перечне компьютеров

    //Обработчик смены выделенной строки в таблице точек доступа
    void onAPRowSelected(int curRow, int curCol, int prevRow, int prevCol);
    //Обработчик смены выделенной строки в таблице рукопожатий
    void onHandshakeRowSelected(int curRow, int curCol, int prevRow, int prevCol);
    //Обработчик смены выделенной строки в списке компьютеров
    void onHostRowSelected(QTreeWidgetItem* current, QTreeWidgetItem* prev);

    void onFileOpen();      //Обработчик нажатия меню Файл\Открыть
    void updateForm();      //Обработчик события обновления содержимого вкладки "Поиск сетей"
    void nextChannel();     //Обработчик события переключения канала
    void snifPacket();      //Обработчик события чтения пакетов из эфира
    void makeDeauth();      //Обработчик события рассылки пакетов деаутентифкации
    void updateSearchProgress();    //Обработчик события прогресса поиска пароля
    
    //Обработчик успешного завершения процесса замера скорости вычислений
    void onHostBenchFinished(int exitCode, QProcess::ExitStatus exitStatus);
    //Обработчик ошибки при запуске процесса замера скорости вычислений
    void onHostBenchError(QProcess::ProcessError error);
    //Обработчик успешного завершения процесса перебора паролей
    void onHostSearchFinished(int exitCode, QProcess::ExitStatus exitStatus);
    //Обработчик ошибки при запуске процесса перебора паролей
    void onHostSearchError(QProcess::ProcessError error);
    //Обработчик чтения данных из процесса перебора пароля (hashcat)
    void onHostSearchReadStdout();

private:
    Ui::MainWindow *ui;     //Конструктор пользовательского интерфейса
    QLabel *packetsLabel;   //Поле в строке статуса с кол-вом пакетов 
    QLabel *channelLabel;   //Поле в строке статуса с номером текущего канала
    int packetCount;        //Счетчик общего кол-ва перехваченных пакетов
    int currentChannel;     //Номер текущего канала
    int maxChannel;         //Общее кол-во каналов на текущем интерфейсе
    QString workDir;        //Путь к папке для хранения дампов и других файлов

    QTimer *updateFormTimer;    //Таймер для обновлений элеменов первой вкладки (списка точетк доступа, кол-ва пакетов)
    QTimer *channelTimer;       //Таймер для переключения между каналами
    QTimer *deauthTimer;        //Таймер для запуска рассылки пакетов деаутентифкации

    BaseSniffer *sniffer;       //Перехватчик пакетов
    SnifferConfiguration snifferConfig; //Настройки для перехватчика
    QTimer *snifferTimer;       //Таймер для перехватчика, работающий в свободное от других обработчиков время 
    NetworkInterface currentIface;  //Сведения о текущем сетевом интерфейсе
    PacketWriter *packetWriter;     //"Писатель" пакетов в файл

    //Информация о клиентской станции
    struct STInfo
    {
        Dot11::address_type stmac;  //MAC-адрес клиента
        WPA_handshake wpa;          //Найденная информаия о рукопожатии
        int wpa_state;              //Флаг состояний заполненности поля wpa 
        int row;                    //Текущий номер строки в таблице клиентов
    };

    //Тип словаря для станций, сопоставляющий MAC-адрес и сведения о станции
    typedef QMap<Dot11::address_type, STInfo> STInfoMap;

    //Данные о каждой точке доступа
    struct APInfo       
    {
        QString essid;              //Название точки
        Dot11::address_type bssid;  //MAC-адрес
        int channel;                //Канал, на котором она работает
        float speed;                //Максимальная поддерживаемая скорость в Мбит\с
        int power;                  //Текущая мощность в dB
        int beaconCount;            //Счетчик пакетов beacon, перехваченных от этой станции
        int row;                    //Текущий номер строки с таблице на форме
        bool handshaked;            //Флаг, указывающий удалось ли перехватить рукопожатие
        int security;               //Флаг с указанием видов шифрования, используемых точкой доступа
        WPA_handshake wpa;          //Найденная информаия о рукопожатии
        STInfoMap clients;          //Словарь со станциями, которые общаются с этой точкой
    };

    //Тип словаря для точек доступа, сопоставляющий MAC-адрес и сведения о точке
    typedef QMap<Dot11::address_type, APInfo> APInfoMap;    
    APInfoMap apInfoMap;    //Объект словаря    

    QList<WPA_handshake> handshakeList;     //Список перехваченных рукопожатий

    //Информация о вычислительном устройстве на компьютере
    struct DeviceInfo
    {
        QString type;           //Тип устройства (CPU, CUDA, OCL)
        float speed;            //Скорость в кол-ве паролей в секунду
        float percent;          //Доля выполненной работы
        QString description;    //Описание устройства
        quint64 skip;           //Кол-во пропускаемых паролей
        quint64 limit;          //Кол-во перебираемых паролей
        QFile *logFile;         //Файл-журнал для записи выводимой процессами информации
        bool statusBegin;       //Флаг используемый при разборе вывода процесса hashcat

        DeviceInfo(const QString &type = QString(), float speed = 0, const QString &desc = QString());
    };

    //Инофрмация о компьютере, на котором будут производиться вычисления
    struct HostInfo
    {
        QString hostName;       //Имя компьютера в сети или IP-адрес
        QString userName;       //Имя пользователя, с правами которого на компьютере будут запускать процессы
        QString hashcatPath;    //Путь к утилите hashcat
        QString cudaPath;       //Путь к утилите cudahashcat
        QString oclPath;        //Путь к утилите oclhashcat
        QList<DeviceInfo> devices;      //Список вычислительных устройств  

        HostInfo(const QString &user = QString(), const QString &host = QString());
    };

    QList<HostInfo> hostList;       //Список компьютеров
    QMap<QProcess*, QPair<int, int>> runningProcesses;  //Словарь, сопоставляющий процесс и номера компьютера и устройства на нем
    bool benchWorking, searchWorking;   //Флаги, указывающие на выполнение в текущий момент замера скорости и перебора паролей
    QString searchEssid;            //Название точки доступа, к которой ищем пароль
    QTime searchStartTime;          //Момент времени, когда начали подбор пароля
    QTimer *searchProgressTimer;    //Таймер для обновления прогресса выполнения перебора
    quint64 searchWorkSize;         //Кол-во вариантов перебираемых паролей

    void initWorkDir();             //Подготавливает текущую папку и вспомогательные файлы
    void initInterfaceList();       //Выводит список сетевых интерфейсов
    void initHandshakeList();       //Подготавливает список перехваченных рукопожатий
    void initHostList();            //Подготавливает список компьютеров

    //Добавялет указанные данные о рукопожатии в список с проверкой на дубликаты
    void appendHandshakeRow(const WPA_handshake &h, bool check);
    //Обновляет содержимое ячейки произвольной таблицы
    void updateTableItem(QTableWidget *tw, int row, int col, const QString &text);
    //Обновляет содержимое строки таблицы точек доступа
    void updateAPRow(const APInfo &info);
    //Обновляет содержимое строки таблицы станций
    void updateClientRow(const STInfo &info);
    //Анализирует содержиое пакета из эфира
    bool processPacket(PDU &pdu);
    //Меняет канал на указанный
    void changeChannel(int channel);
    //По номеру строки ищет указатель на информацию о точке доступа
    APInfoMap::iterator findAPByRow(int row);

    //Добавляет информацию о компьютере в список
    int appendHostRow(const HostInfo &hi);
    //Добавляет информацию об устройстве в список соответствующего компьютера
    int appendDeviceRow(int hostIndex, const DeviceInfo &di);
    //Вызывает диалоговое окно для редактирвоания сведений о компьютере
    bool editWithHostDialog(HostInfo &hi);
    //Обновляет доступность кнопок на второй вкладке
    void updateSearchTabButtons();
    //Рассчивает суммарную скорость всех вычислительных устройств
    double calcHashSumSpeed();
};

#endif // MAINWINDOW_H
