#-------------------------------------------------
#
# Project created by QtCreator 2016-05-02T08:56:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++11

TARGET = WiFiScanner
TEMPLATE = app
LIBS += -ltins

SOURCES += main.cpp\
        mainwindow.cpp \
    utils.cpp

HEADERS  += mainwindow.h \
    utils.h

FORMS    += mainwindow.ui \
    hostdialog.ui
