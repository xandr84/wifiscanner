#include "utils.h"

QStringList callAndWaitProcess(const QString &command, int msecs)
{
    QProcess proc;
    proc.setProcessChannelMode(QProcess::MergedChannels);
    proc.start(command, QIODevice::ReadOnly);
    proc.waitForFinished(msecs);
    return QString(proc.readAllStandardOutput()).split('\n');
}
